package org.minbox.chapter.springboot.integration.using.flyway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootIntegrationUsingFlywayApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootIntegrationUsingFlywayApplication.class, args);
    }

}
