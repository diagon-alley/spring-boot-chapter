package org.minbox.chapter.springboot2.configuration.binding;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 恒宇少年
 */
@RestController
public class TestController {

    @Autowired
    private ConfigProperties configProperties;

    /**
     * 测试输出
     */
    @GetMapping
    public String test() {
        return configProperties.getIpAddress() + ":" + configProperties.getPort();
    }
}
